package com.hendisantika.springbootbcryptexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootBcryptExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootBcryptExampleApplication.class, args);
    }

}

