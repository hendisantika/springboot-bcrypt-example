package com.hendisantika.springbootbcryptexample.repository;

import com.hendisantika.springbootbcryptexample.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}